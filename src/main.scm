;;; main module
(import random-mtzig)

;;; TODO derive version number from CMake top-level input macro
(print "automonster v0.1.0")

;;; initialize RNG
(define st (init))

(let ((hp (modulo (random! st) 100)))
  (print hp))
